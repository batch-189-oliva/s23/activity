/*
WDC028v1.5b-23 | JavaScript - Objects
Graded Activity:

1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
	- Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
	(target.health - this.attack)

2.) If health is less than or equal to 5, invoke faint function


*/

function Pokemon (name, level){
	// propertiese
	this.name = name;
	this.level = name;
	this.health = 10 * level;
	this.attack = 2 * level;

	//Methods
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack))
		target.health = target.health - this.attack
		if (target.health <= 5) {
			target.faint()
		}
	},
	this.faint = function(){
		console.log(this.name + " fainted.")
	}
}

let charizard = new Pokemon ("Charizard", 50);
let lapras = new Pokemon ("Lapras", 40);
let bulbasaur = new Pokemon ("bulbasaur", 15);

console.log(charizard)
console.log(lapras)
console.log(bulbasaur)

console.log("Use tackle by typing pokemonName.tackle(targetPokemonName)")
console.log("Example : charizard.tackle(lapras)")

charizard.tackle(lapras);
